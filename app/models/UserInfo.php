<?php

use Illuminate\Auth\UserInterface;
use Illuminate\Auth\Reminders\RemindableInterface;

class UserInfo extends Eloquent {

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'cat_users_info';
    protected $primaryKey = 'id_usuario_info';
    public $timestamps = true;
    public $softDelete = true;

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = array('password');

  public function contactos() {
        return $this->belongsToMany('TipoContacto', 'cat_users_contacto', 'id_user_info', 'id_tipo_contacto')->withPivot('dato');
    }
    
   

}
