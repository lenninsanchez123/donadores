<?php

class TipoContacto extends Eloquent
{


    protected $table = 'cat_tipo_contacto';
    public $primaryKey = 'id_tipo_contacto';
    public $timestamps = true;
    protected $guarded = array();
    public $incrementing = true;
    protected $perPage = 5;
    public $softDelete = true;

       /**
     * regresa el listado de tipos cntacto
     * @param type $paginate determina si se aplicara paginacion
     */
    public function scopeCustomeAll($query, $paginate = FALSE)
    {
        if ($paginate === true || $paginate == 1) {
            return $query->paginate();
        }
        return $query->get();
    }
    
    public function contactos(){
        return $this->belongsToMany('cat_users_contacto','TipoContacto','id_tipo_contacto','id_user_info')->withPivot('descripcion');
    }
    
 

}
