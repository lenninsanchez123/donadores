<?php

/**
 * @package
 * @category
 * @subpackage
 */
class SincronizacionController extends \BaseController {

    /**
     *  @SWG\Operation(
     *      partial="sincronizacion.index",
     *      summary="Regresa el listado de todos los sincronizacion",
     *      @SWG\Parameter(
     *       name="paginate",
     *       description="Paginar el listado",
     *       required=false,
     *       type="string",
     *       paramType="query",
     *       allowMultiple=false
     *     ),
     *      type="array[users]"
     *  )
     */
    public function index() {
        log::info("listada");
      
        $data=Input::all();
        $informacion=array();
        //log::info($data);
        if(isset($data['categorias'])){
            $informacion['categorias']=Categoria::all()->toArray();
            //log::info(var_export($categorias,true));
            //log::info(var_export($informacion,true));
            log::info("query");
            $last=DB::getQueryLog();
            $la=end($last);
            log::info(var_export($la,true));
            
        }
        
        
        return $informacion;
    }
    
    public function entra(){
        Log::info("entro");
        
    }

    /**
     *  @SWG\Operation(
     *      partial="sincronizacion.store",
     *      summary="Guarda un usuario",
     *      type="users",
     * *     @SWG\Parameter(
     *       name="body",
     *       description="Objeto Usuario que se necesita para guardar",
     *       required=true,
     *       type="User",
     *       paramType="body",
     *       allowMultiple=false
     *     ),
     *  )
     */
    public function store() {
        log::info("datosRecibidos");
        $data = Input::only('usuario');
        DB::beginTransaction();
        try {
            if($data['usuario']) {
                $return = $this->addUser($data['usuario']);
            }
            DB::commit();
            return $return;
        } catch (Exception $ex) {
            
            DB::rollback();
            
        }
    }

    //CREAR UN REGISTRO NUEVO EN USUARIOS
    public function addUser($usuario) {
        log::info("creacion usuario");
        $data['usuario'] = $usuario;
        $nombre = isset($data['usuario']['nombre']) ? $data['usuario']['nombre'] : '';
        $ap = isset($data['usuario']['apellido_paterno']) ? $data['usuario']['apellido_paterno'] : '';
        $am = isset($data['usuario']['apellido_materno']) ? $data['usuario']['apellido_materno'] : '';
        $id_tipo_donador = isset($data['usuario']['id_tipo_donador']) ? $data['usuario']['id_tipo_donador'] : '';
        $donacion = isset($data['usuario']['donacion_info']) ? $data['usuario']['donacion_info'] : '';
        $username = isset($data['usuario']['username']) ? $data['usuario']['username'] : '';
        $password = isset($data['usuario']['password']) ? $data['usuario']['password'] : '';

        //die();
        if (!$data['usuario']) {

            return false;
        }
        $usuario = User::select('username')->where('username', '=', $data['usuario']['username'])->first();

        if (count($usuario) > 0) {
            log::info("dada");
            return Response::json(array('error' => "500", 'error_message' => "Ya existe un usuario con esta cuenta"));
        } else {

           
            try {
                $usuarioInfo = new UserInfo();
                $usuarioInfo->nombre = trim($nombre);
                $usuarioInfo->apellido_paterno = trim($ap);
                $usuarioInfo->apellido_materno = trim($am);
                $usuarioInfo->id_tipo_donador = trim($id_tipo_donador);
                $usuarioInfo->donacion_info = trim($donacion);
                $usuarioInfo->save();
                log::info("inseted");
                log::info($usuarioInfo);
                if ($data['usuario']['contacto']) {
                    $usuario = UserInfo::find($usuarioInfo->id_usuario_info);

                    $arrContactos = array();
                    log::info("find");
                    log::info($usuario);
                    log::info("contacto");
                    log::info($usuario->contactos);
                    $usuario->contactos()->detach();

                    if ($data['usuario']['contacto'] && is_array($data['usuario']['contacto'])) {
                        foreach ($data['usuario']['contacto'] as $value) {
                            $arrContactos[] = array('id_tipo_contacto' => $value['tipo'], 'dato' => $value['valor']);
                        }
                    }
                    $usuario->contactos()->attach($arrContactos);
                }
                $login = new User();
                $login->username = trim($username);
                $login->password = md5(trim($password));
                $login->id_usuario_info = $usuarioInfo->id_usuario_info;
                $login->save();

            
                return Response::json(array('error' => "200", 'error_message' => 'Se han guardado los datos'));
            } catch (Exception $ex) {

                return Response::json(array('error' => "500", 'error_message' => $ex->getMessage()));
              
            }
        }
    }

    public function send($datos, $correo, $id) {


        // Find the user using the user id
        $user = Sentry::findUserById($id);
        $resetCode = $user->getResetPasswordCode();
        $datos = [ 'msg' => $resetCode, 'usuario' => $correo, 'nombre' => $datos['first_name'] . ' ' . $datos['last_name']];
        Mail::send('emails.auth.reminder', $datos, function($message) use($correo, $resetCode) {
            $message->subject('Acceso a sistema SIREM - GEN');
            $message->to($correo);
        });
    }

    /**
     *  @SWG\Operation(
     *      partial="sincronizacion.show",
     *      summary="Muestra el usuario especificado",
     *      @SWG\Parameter(
     *       name="id",
     *       description="Identificador del usuario",
     *       required=true,
     *       type="integer",
     *       paramType="path",
     *       allowMultiple=false
     *     ),
     *  )
     * @return Response
     */
    public function show($fecha) {
        
    }

    /**
     *  @SWG\Operation(
     *      partial="sincronizacion.update",
     *      summary="Actualiza un usuario",
     *      type="User",
     *     @SWG\Parameter(
     *       name="body",
     *       description="Objeto Usuario que se necesita para guardar",
     *       required=true,
     *       type="User",
     *       paramType="body",
     *       allowMultiple=false
     *     ),
     *     @SWG\Parameter(
     *       name="id",
     *       description="Identificador del usuario",
     *       required=true,
     *       type="integer",
     *       paramType="path",
     *       allowMultiple=false
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Id invalido"),
     *     @SWG\ResponseMessage(code=404, message="Usuario no encontrado"),
     *     @SWG\ResponseMessage(code=405, message="Excepción de validación")
     *  )
     */
    public function update($id) {
        
    }

    /**
     *  @SWG\Operation(
     *      partial="sincronizacion.delete",
     *      summary="Elimina el usuario especificado",
     *      @SWG\Parameter(
     *       name="id",
     *       description="Identificador del usuario",
     *       required=true,
     *       type="integer",
     *       paramType="path",
     *       allowMultiple=false,
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Id invalido"),
     *     @SWG\ResponseMessage(code=404, message="Usuario no encontrado")
     *  )
     * @return Response
     */
    public function destroy($id) {
        
    }

}
