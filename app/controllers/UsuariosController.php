<?php

/**
 * @package
 * @category
 * @subpackage
 */
class UsuariosController extends \BaseController {

    /**
     *  @SWG\Operation(
     *      partial="usuarios.index",
     *      summary="Regresa el listado de todos los usuarios",
     *      @SWG\Parameter(
     *       name="paginate",
     *       description="Paginar el listado",
     *       required=false,
     *       type="string",
     *       paramType="query",
     *       allowMultiple=false
     *     ),
     *      type="array[users]"
     *  )
     */
    public function index() {
        log::info("listado");
//        Log::info("ruta");
//
//        $paginate = Input::get('paginate', true);
//        $activos = Input::get('activos');
//        $descripcion = Input::only('descripcion', '');
//        $usuarios = Usuario::customeAll($paginate, $descripcion, $activos);
//        foreach ($usuarios as $usuario) {
//            $usuario->grupos;
//
//
//            $usuario->suspended = Usuario::isSuspended($usuario->id);
//        }
//
//        if ($paginate == 0) {
//            return Response::json(array('data' => $usuarios), '200');
//        } else {
//            return $usuarios;
//        }
    }

    /**
     *  @SWG\Operation(
     *      partial="usuarios.store",
     *      summary="Guarda un usuario",
     *      type="users",
     * *     @SWG\Parameter(
     *       name="body",
     *       description="Objeto Usuario que se necesita para guardar",
     *       required=true,
     *       type="User",
     *       paramType="body",
     *       allowMultiple=false
     *     ),
     *  )
     */
    public function store() {
        log::info("creacion usuario");
        $data = Input::all();
        $nombre = isset($data['usuario']['nombre']) ? $data['usuario']['nombre'] : '';
        $ap = isset($data['usuario']['apellido_paterno']) ? $data['usuario']['apellido_paterno'] : '';
        $am = isset($data['usuario']['apellido_materno']) ? $data['usuario']['apellido_materno'] : '';
        $id_tipo_donador = isset($data['usuario']['id_tipo_donador']) ? $data['usuario']['id_tipo_donador'] : '';
        $donacion = isset($data['usuario']['donacion_info']) ? $data['usuario']['donacion_info'] : '';
        $username = isset($data['usuario']['username'])?$data['usuario']['username']:'';
        $password= isset($data['usuario']['password'])?$data['usuario']['password']:'';
        log::info(var_export($data, true));
        //die();
        if (!$data['usuario']) {
            return false;
        }
        $usuario = User::select('username')->where('username', '=', $data['usuario']['username'])->first();

        if (count($usuario) > 0) {
            return Response::json(array('error' => "500", 'error_message' => "Ya existe un usuario con esta cuenta"));
        } else {
            DB::beginTransaction();
            try {
                $usuarioInfo = new UserInfo();
                $usuarioInfo->nombre = trim($nombre);
                $usuarioInfo->apellido_paterno = trim($ap);
                $usuarioInfo->apellido_materno = trim($am);
                $usuarioInfo->id_tipo_donador = trim($id_tipo_donador);
                $usuarioInfo->donacion_info = trim($donacion);
                $usuarioInfo->save();
                log::info("inseted");
                log::info($usuarioInfo);
                if ($data['usuario']['contacto']) {
                    $usuario = UserInfo::find($usuarioInfo->id_usuario_info);
                    
                    $arrContactos = array();
                    log::info("find");
                    log::info($usuario);
                    log::info("contacto");
                    log::info($usuario->contactos);
                    $usuario->contactos()->detach();

                    if ($data['usuario']['contacto'] && is_array($data['usuario']['contacto'])) {
                        foreach ($data['usuario']['contacto'] as $value) {
                            $arrContactos[] = array('id_tipo_contacto' => $value['tipo'], 'dato' => $value['valor']);
                        }
                    }
                    $usuario->contactos()->attach($arrContactos);
                }
                $login=new User();
                $login->username=trim($username);
                $login->password=md5(trim($password));
                $login->id_usuario_info=$usuarioInfo->id_usuario_info;
                $login->save();
                
                DB::commit();
                  return Response::json(array('error' => "200", 'error_message' => 'Se han guardado los datos'));
            } catch (Exception $ex) {
               
                return Response::json(array('error' => "500", 'error_message' => $ex->getMessage()));
                 DB::rollback();
            }
        }

    }

    public function activa($get) {
        //return View::make('usuarios.activacion')->with('name', 'Steve');

        try {

            $user = Sentry::findUserByResetPasswordCode($get);
            $data['user'] = $user;
            if ($user->activated == 1) {
                return View::make('usuarios.error', $data);
            } else {
                return View::make('usuarios.activacion', $data);
            }
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            //return View('usuarios.activacion', ['name' => 'James']);
            $data['error'] = "Esta cuenta ya ha sido activada";
            return View::make('usuarios.error');
        }
    }

    public function send($datos, $correo, $id) {


        // Find the user using the user id
        $user = Sentry::findUserById($id);
        $resetCode = $user->getResetPasswordCode();
        $datos = [ 'msg' => $resetCode, 'usuario' => $correo, 'nombre' => $datos['first_name'] . ' ' . $datos['last_name']];
        Mail::send('emails.auth.reminder', $datos, function($message) use($correo, $resetCode) {
            $message->subject('Acceso a sistema SIREM - GEN');
            $message->to($correo);
        });
    }

    /**
     *  @SWG\Operation(
     *      partial="usuarios.show",
     *      summary="Muestra el usuario especificado",
     *      @SWG\Parameter(
     *       name="id",
     *       description="Identificador del usuario",
     *       required=true,
     *       type="integer",
     *       paramType="path",
     *       allowMultiple=false
     *     ),
     *  )
     * @return Response
     */
    public function regresaUsuario() {


        try {
            $user = Sentry::findUserByResetPasswordCode(Input::get('codigo'));
        } catch (Cartalyst\Sentry\Users\UserNotFoundException $e) {
            echo 'User was not found.';
        }
        return $user;
    }

    public function show($id) {
        Log::info("show actualizar ");

        $_usuario = Usuario::find($id);

        if (empty($_usuario)) {

            $_usuario = Usuario::withTrashed()->where('id', $id)->first();
        }

        $usuario = Sentry::findUserById($id);
        $usuario->grupos = $_usuario->grupos;
        $empleados = $_usuario->empleado;
        if ($empleados && count($empleados) > 0) {
            $usuario->id_empleado = $empleados[0]->id_empleado;
        }
        $usuario->secciones = $_usuario->secciones;
        $usuario->clientes = $_usuario->clientes;
        return $usuario;
    }

    /**
     *  @SWG\Operation(
     *      partial="usuarios.update",
     *      summary="Actualiza un usuario",
     *      type="User",
     *     @SWG\Parameter(
     *       name="body",
     *       description="Objeto Usuario que se necesita para guardar",
     *       required=true,
     *       type="User",
     *       paramType="body",
     *       allowMultiple=false
     *     ),
     *     @SWG\Parameter(
     *       name="id",
     *       description="Identificador del usuario",
     *       required=true,
     *       type="integer",
     *       paramType="path",
     *       allowMultiple=false
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Id invalido"),
     *     @SWG\ResponseMessage(code=404, message="Usuario no encontrado"),
     *     @SWG\ResponseMessage(code=405, message="Excepción de validación")
     *  )
     */
    public function update($id) {

        Log::info("vengo de edicion usuarios");
        $data = Input::only('email', 'first_name', 'last_name', 'password', 'id_empleado', 'activado', 'permissions', 'grupos', 'clientes', 'activated', 'deleted_at');
        Log::info($data);
        $user = Sentry::findUserById($id);
        // Update the user details



        if (!empty($data['activado']) && $data['activado'] || !empty($data['activated']) && $data['activated']) {
            Log::info("entra al 1 if");
            $user->activated = $data['activado'];
            $user->password = $data['password'];
            //$user->email = $data['email'];
            $apellidos = explode(' ', $user->last_name);
            $total = count($apellidos);
            Log::info($total);
            $empleado['nombre'] = $user->first_name;
            $empleado['apellido_paterno'] = $apellidos[0];
            if ($total > 1) {
                $empleado['apellido_materno'] = $apellidos[1];
            } else {
                $empleado['apellido_materno'] = '';
            }
            if (!$data['id_empleado']) {
                $empleado = Empleado::create($empleado);
            } else {
                $empleado = Empleado::find($data['id_empleado']);
            }
            //$usuario=Usuario::find($user->id);
            $usuario = Usuario::withTrashed()->where('id', $user->id)->first();
            $userId = DB::Table('users')->select('password')->where('id', $user->id)->first();
            log::info("pass");
            log::info($userId->password);
            if (!empty($data['activated']) && $data['activated']) {
                $user->password = $userId->password;
            }
            $user->deleted_at = NULL;
            $user->id_empleado = $empleado->id_empleado;

            $usuario->empleado()->detach();
            $usuario->empleado()->attach(array($empleado->id_empleado));
        } else if ($data['email']) {
            Log::info("entra al 2 if");
            Log::info($data);
            $user->first_name = $data['first_name'];
            $user->last_name = $data['last_name'];
            $user->email = $data['email'];
            $user->permissions = $data['permissions'];
            if (isset($data['grupos'][0])) {
                $data['grupos'] = $data['grupos'][0]['id'];
            } else {
                $data['grupos'] = $data['grupos']['id'];
            }

            Log::info($data['grupos']);
            if (!$data['clientes']) {
                $data['clientes'] = array(1, 2);
            }
            $usuario = Usuario::withTrashed()->where('id', $user->id)->first();
            //$usuario=Usuario::find($user->id);
            $usuario->grupos()->detach();
            $usuario->grupos()->attach($data['grupos']);
        } else if (!empty($data['activated']) && !empty($data['deleted_at'])) {
            Log::info("Entra a activar usuario");
            $user->activated = $data['activated'];
            $user->deleted_at = $data['deleted_at'];
        }

        $user->save();
        return $user;
    }

    /**
     *  @SWG\Operation(
     *      partial="usuarios.delete",
     *      summary="Elimina el usuario especificado",
     *      @SWG\Parameter(
     *       name="id",
     *       description="Identificador del usuario",
     *       required=true,
     *       type="integer",
     *       paramType="path",
     *       allowMultiple=false,
     *     ),
     *     @SWG\ResponseMessage(code=400, message="Id invalido"),
     *     @SWG\ResponseMessage(code=404, message="Usuario no encontrado")
     *  )
     * @return Response
     */
    public function destroy($id) {
        $user = Sentry::findUserById($id);
        $user->activated = false;
        $user->save();
        return Usuario::destroy($id);
    }

    public function unsuspend($id) {
        $response = Usuario::unsuspend($id);
        if ($response === true) {
            return Response::json(array('result' => $response), '200');
        } else {
            return Response::json(array('error' => array(
                            'heading' => 'Error',
                            'message' => $response)), 500);
        }
    }

    public function suspend($id) {
        $response = Usuario::suspend($id);
        if ($response === true) {
            return Response::json(array('result' => $response), '200');
        } else {
            return Response::json(array('error' => array(
                            'heading' => 'Error',
                            'message' => $response)), 500);
        }
    }

}
