<?php 
$I = new TestNinja($scenario);
$I->wantTo('enviar informacion por post');
        $I->amOnPage('/');
$I->submitForm('.contact-form', [
    'name' => 'Mario Basic',
    'email' => 'test@test.com',
    'comment' => 'This is a codeception test email.'
]);
$I->seeCurrentUrlEquals('/contact');
$I->see('Message sent!');

$I->amOnPage('/');
$I->submitForm('.contact-form', [
    'name' => 'Mario Basic',
    'email' => 'testtest.com',
    'comment' => 'This is a codeception test email.'
]);
$I->seeCurrentUrlEquals('/contact');
$I->see('Error sending your message.');
